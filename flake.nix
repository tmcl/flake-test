{
  description = "A very basic flake";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }: 

let 
  makePackage = pkgs: pkgs.haskell.packages.ghc884.callPackage self {};
in rec {
    packages.x86_64-linux.hello = makePackage nixpkgs.legacyPackages.x86_64-linux;
    packages.aarch64-linux.hello = makePackage nixpkgs.legacyPackages.aarch64-linux;
    packages.armv6l-linux.hello = makePackage nixpkgs.legacyPackages.x86_64-linux.pkgsCross.raspberryPi;
    packages.x86_64-linux.cxhello = makePackage nixpkgs.legacyPackages.x86_64-linux.pkgsCross.raspberryPi;

    defaultPackage.x86_64-linux = self.packages.x86_64-linux.hello;
    defaultPackage.armv6l-linux = self.packages.armv6l-linux.hello;
    defaultPackage.aarch64-linux = self.packages.aarch64-linux.hello;

    hydraJobs.hello.x86_64-linux = packages.x86_64-linux.hello;
    hydraJobs.cxhello.x86_64-linux = packages.x86_64-linux.cxhello;

  };
}
